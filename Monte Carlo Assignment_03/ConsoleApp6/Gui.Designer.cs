﻿namespace ConsoleApp6
{
    partial class Gui
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.UnderlyingPrice = new System.Windows.Forms.TextBox();
            this.StrikePrice = new System.Windows.Forms.TextBox();
            this.Tenor = new System.Windows.Forms.TextBox();
            this.Volatility = new System.Windows.Forms.TextBox();
            this.RiskfreeRate = new System.Windows.Forms.TextBox();
            this.Steps = new System.Windows.Forms.TextBox();
            this.Trials = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(30, 26);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(146, 50);
            this.button1.TabIndex = 0;
            this.button1.Text = "Run Simulation";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 104);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Underlying Price";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(47, 140);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Strike Price";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(76, 171);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 15);
            this.label3.TabIndex = 3;
            this.label3.Text = "Tenor";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(63, 202);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 15);
            this.label4.TabIndex = 4;
            this.label4.Text = "Volatility";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(47, 236);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(119, 15);
            this.label5.TabIndex = 5;
            this.label5.Text = "Risk-free Rate";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(76, 267);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 15);
            this.label6.TabIndex = 6;
            this.label6.Text = "Steps";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(76, 298);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 15);
            this.label7.TabIndex = 7;
            this.label7.Text = "Trials";
            // 
            // UnderlyingPrice
            // 
            this.UnderlyingPrice.Location = new System.Drawing.Point(177, 101);
            this.UnderlyingPrice.Name = "UnderlyingPrice";
            this.UnderlyingPrice.Size = new System.Drawing.Size(166, 25);
            this.UnderlyingPrice.TabIndex = 8;
            this.UnderlyingPrice.TextChanged += new System.EventHandler(this.UnderlyingPrice_TextChanged);
            // 
            // StrikePrice
            // 
            this.StrikePrice.Location = new System.Drawing.Point(177, 137);
            this.StrikePrice.Name = "StrikePrice";
            this.StrikePrice.Size = new System.Drawing.Size(166, 25);
            this.StrikePrice.TabIndex = 9;
            this.StrikePrice.TextChanged += new System.EventHandler(this.StrikePrice_TextChanged);
            // 
            // Tenor
            // 
            this.Tenor.Location = new System.Drawing.Point(177, 168);
            this.Tenor.Name = "Tenor";
            this.Tenor.Size = new System.Drawing.Size(166, 25);
            this.Tenor.TabIndex = 10;
            this.Tenor.TextChanged += new System.EventHandler(this.Tenor_TextChanged);
            // 
            // Volatility
            // 
            this.Volatility.Location = new System.Drawing.Point(177, 199);
            this.Volatility.Name = "Volatility";
            this.Volatility.Size = new System.Drawing.Size(166, 25);
            this.Volatility.TabIndex = 11;
            this.Volatility.TextChanged += new System.EventHandler(this.Volatility_TextChanged);
            // 
            // RiskfreeRate
            // 
            this.RiskfreeRate.Location = new System.Drawing.Point(177, 233);
            this.RiskfreeRate.Name = "RiskfreeRate";
            this.RiskfreeRate.Size = new System.Drawing.Size(166, 25);
            this.RiskfreeRate.TabIndex = 12;
            this.RiskfreeRate.TextChanged += new System.EventHandler(this.RiskfreeRate_TextChanged);
            // 
            // Steps
            // 
            this.Steps.Location = new System.Drawing.Point(177, 264);
            this.Steps.Name = "Steps";
            this.Steps.Size = new System.Drawing.Size(166, 25);
            this.Steps.TabIndex = 13;
            this.Steps.TextChanged += new System.EventHandler(this.Steps_TextChanged);
            // 
            // Trials
            // 
            this.Trials.Location = new System.Drawing.Point(177, 295);
            this.Trials.Name = "Trials";
            this.Trials.Size = new System.Drawing.Size(166, 25);
            this.Trials.TabIndex = 14;
            this.Trials.TextChanged += new System.EventHandler(this.Trials_TextChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(359, 26);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(117, 19);
            this.checkBox1.TabIndex = 15;
            this.checkBox1.Text = "Call Option";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(359, 51);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(109, 19);
            this.checkBox2.TabIndex = 16;
            this.checkBox2.Text = "Put Option";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(47, 344);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(109, 19);
            this.checkBox3.TabIndex = 17;
            this.checkBox3.Text = "Antithetic";
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(47, 369);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(149, 19);
            this.checkBox4.TabIndex = 18;
            this.checkBox4.Text = "Control Variate";
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.CheckedChanged += new System.EventHandler(this.checkBox4_CheckedChanged);
            // 
            // Gui
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(517, 451);
            this.Controls.Add(this.checkBox4);
            this.Controls.Add(this.checkBox3);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.Trials);
            this.Controls.Add(this.Steps);
            this.Controls.Add(this.RiskfreeRate);
            this.Controls.Add(this.Volatility);
            this.Controls.Add(this.Tenor);
            this.Controls.Add(this.StrikePrice);
            this.Controls.Add(this.UnderlyingPrice);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Name = "Gui";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox UnderlyingPrice;
        private System.Windows.Forms.TextBox StrikePrice;
        private System.Windows.Forms.TextBox Tenor;
        private System.Windows.Forms.TextBox Volatility;
        private System.Windows.Forms.TextBox RiskfreeRate;
        private System.Windows.Forms.TextBox Steps;
        private System.Windows.Forms.TextBox Trials;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox4;
    }
}