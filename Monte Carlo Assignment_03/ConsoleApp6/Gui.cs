﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Simulation;

namespace ConsoleApp6
{
    public partial class Gui : Form
    {
        public double underlyingprice { get; private set; }
        public double strikeprice { get; private set; }
        public double tenor { get; private set; }
        public double volatility { get; private set; }
        public double riskfree { get; private set; }
        public uint steps { get; private set; }
        public UInt32 trials { get;private set; }

        public Gui()
        {
            InitializeComponent();

        }

        public event Action RunSimulationCall;
        public event Action RunSimulationPut;
        public event Action RunSimulationCall1;
        public event Action RunSimulationPut1;
        public event Action RunSimulationCall2;
        public event Action RunSimulationPut2;
        public event Action RunSimulationCall3;
        public event Action RunSimulationPut3;

        private void button1_Click(object sender, EventArgs e)
        {
                try
                {
                    this.underlyingprice = Convert.ToDouble(UnderlyingPrice.Text);
                    this.strikeprice = Convert.ToDouble(StrikePrice.Text);
                    this.tenor = Convert.ToDouble(Tenor.Text);
                    this.volatility = Convert.ToDouble(Volatility.Text);
                    this.riskfree = Convert.ToDouble(RiskfreeRate.Text);
                    this.steps = Convert.ToUInt16(Steps.Text);
                    this.trials = Convert.ToUInt32(Trials.Text);
                    if (this.underlyingprice < 0 || this.strikeprice < 0 || this.tenor <= 0 || this.volatility < 0 || this.riskfree < 0 || this.steps <= 0 || this.trials <= 0)
                    { MessageBox.Show("Invalid value"); }
                    else
                    {
                      if (checkBox1.Checked == true && checkBox2.Checked == false)
                      {
                        if (checkBox3.Checked == true && checkBox4.Checked == false)
                        {
                            if (RunSimulationCall1 != null)
                                RunSimulationCall1();                           
                        }
                        else if (checkBox3.Checked == false && checkBox4.Checked == false)
                        {
                            if (RunSimulationCall != null)
                                RunSimulationCall();
                        }
                        else if (checkBox3.Checked == false && checkBox4.Checked == true)
                        {
                            if (RunSimulationCall2 != null)
                                RunSimulationCall2();
                        }
                        else if (checkBox3.Checked == true && checkBox4.Checked == true)
                        {
                            if (RunSimulationCall3 != null)
                                RunSimulationCall3();
                        }
                      }
                     else if (checkBox1.Checked == false && checkBox2.Checked == true)
                     {
                        if (checkBox3.Checked == true && checkBox4.Checked == false)
                        {
                            if (RunSimulationPut1 != null)
                                RunSimulationPut1();
                        }
                        else if(checkBox3.Checked == false && checkBox4.Checked == false)
                        {
                            if (RunSimulationPut != null)
                                RunSimulationPut();
                        }
                        else if (checkBox3.Checked == false && checkBox4.Checked == true)
                        {
                            if (RunSimulationPut2 != null)
                                RunSimulationPut2();
                        }
                        else if (checkBox3.Checked == true && checkBox4.Checked == true)
                        {
                            if (RunSimulationPut3 != null)
                                RunSimulationPut3();
                        }
                     }
                        else
                        {
                            MessageBox.Show("Invalid Choice");
                        }
                    }
                }
                catch (FormatException)
                {
                    MessageBox.Show("Invalid value");
                }
        }

        private void UnderlyingPrice_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void StrikePrice_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void Tenor_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void Volatility_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void RiskfreeRate_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void Steps_TextChanged(object sender, EventArgs e)
        {
            

        }

        private void Trials_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }
    }
}
