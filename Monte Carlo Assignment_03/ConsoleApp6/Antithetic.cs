﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Antithetic 
{
    public class Input1
    {
        public double r { get; set; }
        public double vol { get; set; }
        public double t { get; set; }
        public double S { get; set; }
        public static double K { get; set; }
        public static uint step { get; set; }
    }

    public class SimulationPrice1
    {
        public double[] optionprice { get; set; }
        public double[] optioninverse { get; set; }
        public double Price(double S, double r, double vol, double t, double ep)
        {
            double price;
            double deltaT = t / Convert.ToDouble(Input1.step);
            price = S * Math.Exp((r - Math.Pow(vol, 2) / 2) * deltaT + vol * Math.Sqrt(deltaT) * ep);
            return price;
        }

    }

    public class Simulator1
    {
        SimulationPrice1 simulationprice = new SimulationPrice1();
        public static UInt32 trials { get; set; }
        public List<double> PriceList;
        public List<double> PriceInverse;
        public static double[,] RandomList1;
        public static double[,] RandomList2;

        public void Initial()
        {
            simulationprice.optionprice = new double[Input1.step+1];
            simulationprice.optioninverse = new double[Input1.step+1];
            PriceList = new List<double>();
            PriceInverse = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList1 = new double[trials, Input1.step];
            for (UInt32 i = 0; i < trials; i++)
            {
                for (int k = 0; k < Input1.step; k++)
                {
                    RandomList1[i, k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public void RandomAntithetic()
        {
            RandomList2 = new double[trials, Input1.step];
            for (UInt32 i=0;i<trials;i++)
            {
                for (int k=0;k<Input1.step;k++)
                {
                    RandomList2[i, k] = -RandomList1[i, k];
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < trials; i++)
            {
                simulationprice.optionprice[0] = S;
                for (int j = 1; j < Input1.step+1; j++)
                {
                    simulationprice.optionprice[j] = simulationprice.Price(simulationprice.optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
                PriceList.Add(simulationprice.optionprice.Last());
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < trials; i++)
            {
                simulationprice.optioninverse[0] = S;
                for (int j = 1; j < Input1.step+1; j++)
                {
                    simulationprice.optioninverse[j] = simulationprice.Price(simulationprice.optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
                PriceInverse.Add(simulationprice.optioninverse.Last());
            }
            List<double> Option1 = new List<double>();
            List<double> Option2 = new List<double>();
            for (UInt32 i = 0; i < PriceList.Count; i++)
            {
                Option1.Add(Math.Max(PriceList[(int)i] - Input1.K, 0));
            }
            for (UInt32 i = 0; i < PriceInverse.Count; i++)
            {
                Option2.Add(Math.Max(PriceInverse[(int)i] - Input1.K, 0));
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option1.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Option2.Average();
            return (price1+price2)/2;
        }
        
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < trials; i++)
            {
                simulationprice.optionprice[0] = S;
                for (uint j = 1; j < Input1.step+1; j++)
                {
                    simulationprice.optionprice[j] = simulationprice.Price(simulationprice.optionprice[j - 1], r, vol, t, RandomList1[i, j - 1]);
                }
                PriceList.Add(simulationprice.optionprice.Last());
            }
            RandomAntithetic();
            for (UInt32 i = 0; i < trials; i++)
            {
                simulationprice.optioninverse[0] = S;
                for (int j = 1; j < Input1.step+1; j++)
                {
                    simulationprice.optioninverse[j] = simulationprice.Price(simulationprice.optioninverse[j - 1], r, vol, t, RandomList2[i, j - 1]);
                }
                PriceInverse.Add(simulationprice.optioninverse.Last());
            }
            List<double> Option1 = new List<double>();
            List<double> Option2 = new List<double>();
            for (UInt32 i = 0; i < PriceList.Count; i++)
            {
                Option1.Add(Math.Max(Input1.K - PriceList[(int)i], 0));
            }
            for (UInt32 i = 0; i < PriceInverse.Count; i++)
            {
                Option2.Add(Math.Max(Input1.K - PriceInverse[(int)i], 0));
            }
            double price1 = 0;
            price1 = Math.Exp(-r * t) * Option1.Average();
            double price2 = 0;
            price2 = Math.Exp(-r * t) * Option2.Average();
            return (price1 + price2) / 2;
        }

        public double CallStandardError()
        {
            List<double> Option1 = new List<double>();
            List<double> Option2 = new List<double>();
            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < PriceList.Count; i++)
            {
                Option1.Add(Math.Max(PriceList[(int)i] - Input1.K, 0));
            }
            for (UInt32 i = 0; i < PriceInverse.Count; i++)
            {
                Option2.Add(Math.Max(PriceInverse[(int)i] - Input1.K, 0));
            }
            for (UInt32 i =0;i<PriceList.Count;i++)
            {
                Option.Add((Option1[(int)i] + Option2[(int)i]) / 2);
            }
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }

        public double PutStandardError()
        {
            List<double> Option1 = new List<double>();
            List<double> Option2 = new List<double>();
            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < PriceList.Count; i++)
            {
                Option1.Add(Math.Max(Input1.K - PriceList[(int)i], 0));
            }
            for (UInt32 i = 0; i < PriceInverse.Count; i++)
            {
                Option2.Add(Math.Max(Input1.K - PriceInverse[(int)i], 0));
            }
            for (UInt32 i = 0; i < PriceList.Count; i++)
            {
                Option.Add((Option1[(int)i] + Option2[(int)i]) / 2);
            }
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
    }

    public class Greek1
    {
        public double calldelta(double S, double r, double vol, double t)
        {
            Simulator1 sim = new Simulator1();
            double cp1 = sim.CallPrice(S + 0.0001, r, vol, t);
            double cp2 = sim.CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S, double r, double vol, double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S , r, vol, t)) / 0.01;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            Simulator1 sim = new Simulator1();
            double cp1 = sim.CallPrice(S, r, vol + 0.0001, t);
            double cp2 = sim.CallPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S, double r, double vol, double t)
        {
            Simulator1 sim = new Simulator1();
            double cp1 = sim.CallPrice(S, r, vol, t + 0.0001);
            double cp2 = sim.CallPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            Simulator1 sim = new Simulator1();
            double cp1 = sim.CallPrice(S, r + 0.0001, vol, t);
            double cp2 = sim.CallPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            Simulator1 sim = new Simulator1();
            double cp1 = sim.PutPrice(S + 0.0001, r, vol, t);
            double cp2 = sim.PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            Simulator1 sim = new Simulator1();
            double cp1 = sim.PutPrice(S, r, vol + 0.0001, t);
            double cp2 = sim.PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            Simulator1 sim = new Simulator1();
            double cp1 = sim.PutPrice(S, r, vol, t + 0.0001);
            double cp2 = sim.PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            Simulator1 sim = new Simulator1();
            double cp1 = sim.PutPrice(S, r + 0.0001, vol, t);
            double cp2 = sim.PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }
    internal static class GaussianBoxMuller
    {
        public static double NextDouble()
        {
            double x, y, square;

            do
            {
                x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                square = (x * x) + (y * y);
            } while (square >= 1);

            return x * Math.Sqrt(-2 * Math.Log(square) / square);
        }
    }
    internal static class RandomProvider
    {
        private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
            new Random(Interlocked.Increment(ref seed))
        );

        private static int seed = Environment.TickCount;

        public static Random GetThreadRandom()
        {
            return randomWrapper.Value;
        }
    }
}
