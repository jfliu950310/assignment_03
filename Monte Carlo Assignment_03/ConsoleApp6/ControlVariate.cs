﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ControlVariate
{
        public class Input2
        {
            public double r { get; set; }
            public double vol { get; set; }
            public double t { get; set; }
            public double S { get; set; }
            public static double K { get; set; }
            public static uint step { get; set; }
        }

        public class SimulationPrice2
        {
            public double[] optionprice { get; set; }
            public double[] controlvariate { get; set; }
            public double Price(double S, double r, double vol, double t, double ep)
            {
                double price;
                double deltaT = t / Convert.ToDouble(Input2.step);
                price = S * Math.Exp((r - Math.Pow(vol, 2) / 2) * deltaT + vol * Math.Sqrt(deltaT) * ep);
                return price;
            }
        }

        public class Simulator2
        {
            SimulationPrice2 simulationprice = new SimulationPrice2();
            public static UInt32 trials { get; set; }
            public List<double> PriceList;
            public static double[,] RandomList;

            public void Initial()
            {
                simulationprice.optionprice = new double[Input2.step + 1];
                simulationprice.controlvariate = new double[trials];
                PriceList = new List<double>();
            }
            public void RandomGenerated()
            {
                RandomList = new double[trials, Input2.step];
                for (UInt32 i = 0; i < trials; i++)
                {
                    for (int k = 0; k < Input2.step; k++)
                    {
                        RandomList[i, k] = GaussianBoxMuller.NextDouble();
                    }
                }
            }
            public double CallPrice(double S, double r, double vol, double t)
            {
                Initial();
                double tl;
                double d1;
                double delta;
                double deltaT = t / Convert.ToDouble(Input2.step);
                for (UInt32 i = 0; i < trials; i++)
                {
                    simulationprice.optionprice[0] = S;
                    double cv = 0;
                    for (int j = 1; j < Input2.step + 1; j++)
                    {
                        simulationprice.optionprice[j] = simulationprice.Price(simulationprice.optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                        tl = t - (j-1) * deltaT;
                        d1 = (Math.Log(simulationprice.optionprice[j - 1] / Input2.K) + (r + Math.Pow(vol, 2) / 2)*tl) / (vol * Math.Sqrt(tl));
                        delta = normalCDF(d1);
                        cv = cv + delta * (simulationprice.optionprice[j] - simulationprice.optionprice[j - 1] * Math.Exp(r * deltaT));
                    }
                    simulationprice.controlvariate[i] = -1 * cv;
                    PriceList.Add(simulationprice.optionprice.Last());
                }
                List<double> Option = new List<double>();
                for (UInt32 i = 0; i < PriceList.Count; i++)
                {
                    Option.Add(Math.Max(PriceList[(int)i] - Input2.K, 0) + simulationprice.controlvariate[i]);
                }
                double price = 0;
                price = Math.Exp(-r * t) * Option.Average();
                return price;
            }

            public double PutPrice(double S, double r, double vol, double t)
            {
                Initial();
                double tl;
                double d1;
                double delta;
                double deltaT = t / Convert.ToDouble(Input2.step);
                for (UInt32 i = 0; i < trials; i++)
                {
                    simulationprice.optionprice[0] = S;
                    double cv = 0;
                    for (uint j = 1; j < Input2.step + 1; j++)
                    {
                        simulationprice.optionprice[j] = simulationprice.Price(simulationprice.optionprice[j - 1], r, vol, t, RandomList[i, j - 1]);
                        tl = t - (j - 1) * deltaT;
                        d1 = (Math.Log(simulationprice.optionprice[j - 1] / Input2.K) + (r + Math.Pow(vol, 2) / 2) * tl) / (vol * Math.Sqrt(tl));
                        delta = normalCDF(d1)-1;
                        cv = cv + delta * (simulationprice.optionprice[j] - simulationprice.optionprice[j - 1] * Math.Exp(r * deltaT));
                    }
                    simulationprice.controlvariate[i] = -1 * cv;
                    PriceList.Add(simulationprice.optionprice.Last());
                }
                List<double> Option = new List<double>();
                for (UInt32 i = 0; i < PriceList.Count; i++)
                {
                    Option.Add(Math.Max(Input2.K - PriceList[(int)i], 0) + simulationprice.controlvariate[i]);
                }
                double price = 0;
                price = Math.Exp(-r * t) * Option.Average();
                return price;
            }
            public double CallStandardError()
            {
                List<double> Option = new List<double>();
                for (UInt32 i = 0; i < PriceList.Count; i++)
                {
                    Option.Add(Math.Max(PriceList[(int)i] - Input2.K, 0) + simulationprice.controlvariate[i]);
                }
                double avg = Option.Average();
                double sum = Option.Sum(v => Math.Pow(v - avg, 2));
                return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
            }
            public double PutStandardError()
            {
                List<double> Option = new List<double>();
                for (UInt32 i = 0; i < PriceList.Count; i++)
                {
                    Option.Add(Math.Max(Input2.K - PriceList[(int)i], 0) + simulationprice.controlvariate[i]);
                }
                double avg = Option.Average();
                double sum = Option.Sum(v => Math.Pow(v - avg, 2));
                return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
            }

            public double normalCDF(double value)
            {
                const double A1 = 0.254829592;
                const double A2 = -0.284496736;
                const double A3 = 1.421413741;
                const double A4 = -1.453152027;
                const double A5 = 1.061405429;
                const double P = 0.3275911;

            int sign;
            if (value < 0)
                sign = -1;
            else
                sign = 1;

            double x = Math.Abs(value) / Math.Sqrt(2);
            double t = 1 / (1 + P * x);
            double erf = 1 - (((((A5 * t + A4) * t + A3) * t + A2) * t + A1) * t * Math.Exp(-x * x));


                return 0.5*(1+sign*erf);
            }
        }

        public class Greek2
        {
            public double calldelta(double S, double r, double vol, double t)
            {
                Simulator2 sim = new Simulator2();
                double cp1 = sim.CallPrice(S + 0.0001, r, vol, t);
                double cp2 = sim.CallPrice(S - 0.0001, r, vol, t);
                return (cp1 - cp2) / 0.0002;
            }
            public double callgamma(double S, double r, double vol, double t)
            {
                return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01;
            }
            public double callvega(double S, double r, double vol, double t)
            {
                Simulator2 sim = new Simulator2();
                double cp1 = sim.CallPrice(S, r, vol + 0.0001, t);
                double cp2 = sim.CallPrice(S, r, vol - 0.0001, t);
                return (cp1 - cp2) / 0.0002;
            }
            public double calltheta(double S, double r, double vol, double t)
            {
                Simulator2 sim = new Simulator2();
                double cp1 = sim.CallPrice(S, r, vol, t + 0.0001);
                double cp2 = sim.CallPrice(S, r, vol, t - 0.0001);
                return (cp1 - cp2) / 0.0002;
            }
            public double callrho(double S, double r, double vol, double t)
            {
                Simulator2 sim = new Simulator2();
                double cp1 = sim.CallPrice(S, r + 0.0001, vol, t);
                double cp2 = sim.CallPrice(S, r - 0.0001, vol, t);
                return (cp1 - cp2) / 0.0002;
            }
            public double putdelta(double S, double r, double vol, double t)
            {
                Simulator2 sim = new Simulator2();
                double cp1 = sim.PutPrice(S + 0.0001, r, vol, t);
                double cp2 = sim.PutPrice(S - 0.0001, r, vol, t);
                return (cp1 - cp2) / 0.0002;
            }
            public double putgamma(double S, double r, double vol, double t)
            {
                return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
            }
            public double putvega(double S, double r, double vol, double t)
            {
                Simulator2 sim = new Simulator2();
                double cp1 = sim.PutPrice(S, r, vol + 0.0001, t);
                double cp2 = sim.PutPrice(S, r, vol - 0.0001, t);
                return (cp1 - cp2) / 0.0002;
            }
            public double puttheta(double S, double r, double vol, double t)
            {
                Simulator2 sim = new Simulator2();
                double cp1 = sim.PutPrice(S, r, vol, t + 0.0001);
                double cp2 = sim.PutPrice(S, r, vol, t - 0.0001);
                return (cp1 - cp2) / 0.0002;
            }
            public double putrho(double S, double r, double vol, double t)
            {
                Simulator2 sim = new Simulator2();
                double cp1 = sim.PutPrice(S, r + 0.0001, vol, t);
                double cp2 = sim.PutPrice(S, r - 0.0001, vol, t);
                return (cp1 - cp2) / 0.0002;
            }
        }
        internal static class GaussianBoxMuller
        {
            public static double NextDouble()
            {
                double x, y, square;

                do
                {
                    x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                    square = (x * x) + (y * y);
                } while (square >= 1);

                return x * Math.Sqrt(-2 * Math.Log(square) / square);
            }
        }
        internal static class RandomProvider
        {
            private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
                new Random(Interlocked.Increment(ref seed))
            );

            private static int seed = Environment.TickCount * 10;

            public static Random GetThreadRandom()
            {
                return randomWrapper.Value;
            }
        }

        
    }
