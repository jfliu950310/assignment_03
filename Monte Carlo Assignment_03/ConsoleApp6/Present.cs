﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antithetic;
using Simulation;
using ControlVariate;
using AntitheticCV;
using System.Windows.Forms;
using System.Diagnostics;

namespace ConsoleApp6
{
    public class Present
    {
        private Simulator simulator1;
        private Simulator1 simulator2;
        private Simulator2 simulator3;
        private Simulator3 simulator4;
        private Gui gui1;
        private Greek greek;
        private Greek1 greek1;
        private Greek2 greek2;
        private Greek3 greek3;
        public Gui gui2 { get { return this.gui1; } }
        Stopwatch watch = new Stopwatch();

        public Present(Simulator Simulator, Simulator1 Simulator1, Simulator2 Simulator2, Simulator3 Simulator3, Gui GUI, Greek Greek, Greek1 Greek1,Greek2 Greek2, Greek3 Greek3)
        {
            this.simulator1 = Simulator;
            this.simulator2 = Simulator1;
            this.simulator3 = Simulator2;
            this.simulator4 = Simulator3;
            this.greek = Greek;
            this.greek1 = Greek1;
            this.greek2 = Greek2;
            this.greek3 = Greek3;
            this.gui1 = GUI;
            this.gui1.RunSimulationCall += this.RunSimulationCall;
            this.gui1.RunSimulationPut += this.RunSimulationPut;
            this.gui1.RunSimulationCall1 += this.RunSimulationCall1;
            this.gui1.RunSimulationPut1 += this.RunSimulationPut1;
            this.gui1.RunSimulationCall2 += this.RunSimulationCall2;
            this.gui1.RunSimulationPut2 += this.RunSimulationPut2;
            this.gui1.RunSimulationCall3 += this.RunSimulationCall3;
            this.gui1.RunSimulationPut3 += this.RunSimulationPut3;
        }
        public void RunSimulationCall()
        {
            watch.Start();
            Input input =new Input();
            input.r = this.gui1.riskfree;
            input.vol = this.gui1.volatility;
            input.t = this.gui1.tenor;
            input.S = this.gui1.underlyingprice;
            Input.K = this.gui1.strikeprice;
            Input.step = this.gui1.steps+1;
            Simulator.trials = this.gui1.trials;
            simulator1.RandomGenerated();
            double CallPrice = this.simulator1.CallPrice(input.S, input.r, input.vol, input.t);
            double CallSE = this.simulator1.CallStandardError();
            double calldelta = this.greek.calldelta(input.S, input.r, input.vol, input.t);
            double callgamma = this.greek.callgamma(input.S, input.r, input.vol, input.t);
            double calltheta = this.greek.calltheta(input.S, input.r, input.vol, input.t);
            double callvega = this.greek.callvega(input.S, input.r, input.vol, input.t);
            double callrho = this.greek.callrho(input.S, input.r, input.vol, input.t);
            watch.Stop();
            MessageBox.Show("  Call option price is  " + CallPrice + "\n"
                  + " Standard Error is " + CallSE + "\n"
                 + " the Delta is  " + calldelta + "\n"
                 + " the Gamma is  " + callgamma + "\n"
                 + " the Theta is  " + calltheta + "\n"
                 + " the Vega is  " + callvega + "\n"
                 + " the rho is  " + callrho + "\n"
                 + "Running Time  " + ": " + watch.Elapsed.Seconds.ToString() + ":" + watch.Elapsed.Milliseconds.ToString() + ".");
            watch.Reset();
        }

        public void RunSimulationPut()
        {
            watch.Start();
            Input input = new Input();
            input.r = this.gui1.riskfree;
            input.vol = this.gui1.volatility;
            input.t = this.gui1.tenor;
            input.S = this.gui1.underlyingprice;
            Input.K = this.gui1.strikeprice;
            Input.step = this.gui1.steps+1;
            Simulator.trials = this.gui1.trials;
            simulator1.RandomGenerated();
            double PutPrice = this.simulator1.PutPrice(input.S, input.r, input.vol, input.t);
            double PutSE = this.simulator1.PutStandardError();
            double putdelta = this.greek.putdelta(input.S, input.r, input.vol, input.t);
            double putgamma = this.greek.putgamma(input.S, input.r, input.vol, input.t);
            double puttheta = this.greek.puttheta(input.S, input.r, input.vol, input.t);
            double putvega = this.greek.putvega(input.S, input.r, input.vol, input.t);
            double putrho = this.greek.putrho(input.S, input.r, input.vol, input.t);
            watch.Stop();
            MessageBox.Show("Put option price is" + PutPrice + "\n"
                  + " Standard Error is " + PutSE + "\n"
                + "the Delta is" + putdelta + "\n"
                 + " the Gamma is  " + putgamma + "\n"
                 + " the Theta is  " + puttheta + "\n"
                 + " the Vega is  " + putvega + "\n"
                 + " the rho is  " + putrho + "\n"
                 + "Running Time  " + ": " + watch.Elapsed.Seconds.ToString() + ":" + watch.Elapsed.Milliseconds.ToString() + ".");
            watch.Reset();

        }
        public void RunSimulationCall1()
        {
            watch.Start();
            Input1 input = new Input1();
            input.r = this.gui1.riskfree;
            input.vol = this.gui1.volatility;
            input.t = this.gui1.tenor;
            input.S = this.gui1.underlyingprice;
            Input1.K = this.gui1.strikeprice;
            Input1.step = this.gui1.steps + 1;
            Simulator1.trials = this.gui1.trials;
            simulator2.RandomGenerated();
            double CallPrice = this.simulator2.CallPrice(input.S, input.r, input.vol, input.t);
            double CallSE = this.simulator2.CallStandardError();
            double calldelta = this.greek1.calldelta(input.S, input.r, input.vol, input.t);
            double callgamma = this.greek1.callgamma(input.S, input.r, input.vol, input.t);
            double calltheta = this.greek1.calltheta(input.S, input.r, input.vol, input.t);
            double callvega = this.greek1.callvega(input.S, input.r, input.vol, input.t);
            double callrho = this.greek1.callrho(input.S, input.r, input.vol, input.t);
            watch.Stop();
            MessageBox.Show("  Call option price is  " + CallPrice + "\n"
                  + " Standard Error is " + CallSE + "\n"
                 + " the Delta is  " + calldelta + "\n"
                 + " the Gamma is  " + callgamma + "\n"
                 + " the Theta is  " + calltheta + "\n"
                 + " the Vega is  " + callvega + "\n"
                 + " the rho is  " + callrho + "\n"
                 + "Running Time  " + ": " + watch.Elapsed.Seconds.ToString() + ":" + watch.Elapsed.Milliseconds.ToString() + ".");
            watch.Reset();
        }
        public void RunSimulationPut1()
        {
            watch.Start();
            Input1 input = new Input1();
            input.r = this.gui1.riskfree;
            input.vol = this.gui1.volatility;
            input.t = this.gui1.tenor;
            input.S = this.gui1.underlyingprice;
            Input1.K = this.gui1.strikeprice;
            Input1.step = this.gui1.steps + 1;
            Simulator1.trials = this.gui1.trials;
            simulator2.RandomGenerated();
            double PutPrice = this.simulator2.PutPrice(input.S, input.r, input.vol, input.t);
            double PutSE = this.simulator2.PutStandardError();
            double putdelta = this.greek1.putdelta(input.S, input.r, input.vol, input.t);
            double putgamma = this.greek1.putgamma(input.S, input.r, input.vol, input.t);
            double puttheta = this.greek1.puttheta(input.S, input.r, input.vol, input.t);
            double putvega = this.greek1.putvega(input.S, input.r, input.vol, input.t);
            double putrho = this.greek1.putrho(input.S, input.r, input.vol, input.t);
            watch.Stop();
            MessageBox.Show("Put option price is" + PutPrice + "\n"
                  + " Standard Error is " + PutSE + "\n"
                + "the Delta is" + putdelta + "\n"
                 + " the Gamma is  " + putgamma + "\n"
                 + " the Theta is  " + puttheta + "\n"
                 + " the Vega is  " + putvega + "\n"
                 + " the rho is  " + putrho + "\n"
                 + "Running Time  " + ": " + watch.Elapsed.Seconds.ToString() + ":" + watch.Elapsed.Milliseconds.ToString() + ".");
            watch.Reset();
        }

        public void RunSimulationCall2()
        {
            watch.Start();
            Input2 input = new Input2();
            input.r = this.gui1.riskfree;
            input.vol = this.gui1.volatility;
            input.t = this.gui1.tenor;
            input.S = this.gui1.underlyingprice;
            Input2.K = this.gui1.strikeprice;
            Input2.step = this.gui1.steps;
            Simulator2.trials = this.gui1.trials;
            simulator3.RandomGenerated();
            double CallPrice = this.simulator3.CallPrice(input.S, input.r, input.vol, input.t);
            double CallSE = this.simulator3.CallStandardError();
            double calldelta = this.greek2.calldelta(input.S, input.r, input.vol, input.t);
            double callgamma = this.greek2.callgamma(input.S, input.r, input.vol, input.t);
            double calltheta = this.greek2.calltheta(input.S, input.r, input.vol, input.t);
            double callvega = this.greek2.callvega(input.S, input.r, input.vol, input.t);
            double callrho = this.greek2.callrho(input.S, input.r, input.vol, input.t);
            watch.Stop();
            MessageBox.Show("  Call option price is  " + CallPrice + "\n"
                  + " Standard Error is " + CallSE + "\n"
                 + " the Delta is  " + calldelta + "\n"
                 + " the Gamma is  " + callgamma + "\n"
                 + " the Theta is  " + calltheta + "\n"
                 + " the Vega is  " + callvega+ "\n"
                 + " the rho is  " + callrho + "\n"
                 + "Running Time  " + ": " + watch.Elapsed.Seconds.ToString() + ":" + watch.Elapsed.Milliseconds.ToString() + ".");
            watch.Reset();
        }
        public void RunSimulationPut2()
        {
            watch.Start();
            Input2 input = new Input2();
            input.r = this.gui1.riskfree;
            input.vol = this.gui1.volatility;
            input.t = this.gui1.tenor;
            input.S = this.gui1.underlyingprice;
            Input2.K = this.gui1.strikeprice;
            Input2.step = this.gui1.steps;
            Simulator2.trials = this.gui1.trials;
            simulator3.RandomGenerated();
            double PutPrice = this.simulator3.PutPrice(input.S, input.r, input.vol, input.t);
            double PutSE = this.simulator3.PutStandardError();
            double putdelta = this.greek2.putdelta(input.S, input.r, input.vol, input.t);
            double putgamma = this.greek2.putgamma(input.S, input.r, input.vol, input.t);
            double puttheta = this.greek2.puttheta(input.S, input.r, input.vol, input.t);
            double putvega = this.greek2.putvega(input.S, input.r, input.vol, input.t);
            double putrho = this.greek2.putrho(input.S, input.r, input.vol, input.t);
            watch.Stop();
            MessageBox.Show("Put option price is" + PutPrice+ "\n"
                  + " Standard Error is " + PutSE + "\n"
                + "the Delta is" + putdelta + "\n"
                 + " the Gamma is  " + putgamma+ "\n"
                 + " the Theta is  " + puttheta + "\n"
                 + " the Vega is  " + putvega + "\n"
                 + " the rho is  " + putrho +"\n"
                 + "Running Time  " + ": " + watch.Elapsed.Seconds.ToString() + ":" + watch.Elapsed.Milliseconds.ToString() + ".");
            watch.Reset();

        }

        public void RunSimulationCall3()
        {
            watch.Start();
            Input3 input = new Input3();
            input.r = this.gui1.riskfree;
            input.vol = this.gui1.volatility;
            input.t = this.gui1.tenor;
            input.S = this.gui1.underlyingprice;
            Input3.K = this.gui1.strikeprice;
            Input3.step = this.gui1.steps;
            Simulator3.trials = this.gui1.trials;
            simulator4.RandomGenerated();
            double CallPrice = this.simulator4.CallPrice(input.S, input.r, input.vol, input.t);
            double CallSE = this.simulator4.CallStandardError();
            double calldelta = this.greek3.calldelta(input.S, input.r, input.vol, input.t);
            double callgamma = this.greek3.callgamma(input.S, input.r, input.vol, input.t);
            double calltheta = this.greek3.calltheta(input.S, input.r, input.vol, input.t);
            double callvega = this.greek3.callvega(input.S, input.r, input.vol, input.t);
            double callrho = this.greek3.callrho(input.S, input.r, input.vol, input.t);
            watch.Stop();
            MessageBox.Show("  Call option price is  " + CallPrice + "\n"
                  + " Standard Error is " + CallSE + "\n"
                 + " the Delta is  " + calldelta + "\n"
                 + " the Gamma is  " + callgamma + "\n"
                 + " the Theta is  " + calltheta + "\n"
                 + " the Vega is  " + callvega + "\n"
                 + " the rho is  " + callrho +"\n"
                 + "Running Time  " + ": " + watch.Elapsed.Seconds.ToString() + ":" + watch.Elapsed.Milliseconds.ToString() + ".");
            watch.Reset();
        }

        public void RunSimulationPut3()
        {
            watch.Start();
            Input3 input = new Input3();
            input.r = this.gui1.riskfree;
            input.vol = this.gui1.volatility;
            input.t = this.gui1.tenor;
            input.S = this.gui1.underlyingprice;
            Input3.K = this.gui1.strikeprice;
            Input3.step = this.gui1.steps;
            Simulator3.trials = this.gui1.trials;
            simulator4.RandomGenerated();
            double PutPrice = this.simulator4.PutPrice(input.S, input.r, input.vol, input.t);
            double PutSE = this.simulator4.PutStandardError();
            double putdelta = this.greek3.putdelta(input.S, input.r, input.vol, input.t);
            double putgamma = this.greek3.putgamma(input.S, input.r, input.vol, input.t);
            double puttheta = this.greek3.puttheta(input.S, input.r, input.vol, input.t);
            double putvega = this.greek3.putvega(input.S, input.r, input.vol, input.t);
            double putrho = this.greek3.putrho(input.S, input.r, input.vol, input.t);
            watch.Stop();
            MessageBox.Show("Put option price is" + PutPrice + "\n"
                  + " Standard Error is " + PutSE + "\n"
                + "the Delta is" + putdelta+ "\n"
                 + " the Gamma is  " + putgamma + "\n"
                 + " the Theta is  " + puttheta + "\n"
                 + " the Vega is  " + putvega + "\n"
                 + " the rho is  " + putrho +"\n"
                 + "Running Time  " + ": " + watch.Elapsed.Seconds.ToString() + ":" + watch.Elapsed.Milliseconds.ToString() + ".");
            watch.Reset();

        }
    }

}


