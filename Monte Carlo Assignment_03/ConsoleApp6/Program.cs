﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Simulation;
using Antithetic;
using AntitheticCV;
using ControlVariate;

namespace ConsoleApp6
{
    static class Program
    {
        static void Main()
        {
            Present presenter = new Present(new Simulator(), new Simulator1(),new Simulator2(),new Simulator3(),new Gui(), new Greek(),new Greek1(), new Greek2(),new Greek3());
            Application.Run(presenter.gui2);
        }
    }
}
