﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Simulation
{
    public class Input
    {
        public double r { get; set; }
        public double vol { get; set; }
        public double t { get; set; }
        public double S { get; set; }
        public static double K { get; set; }
        public static uint step { get; set; }
    }

    public class SimulationPrice
    {
        public double[] optionprice { get; set; }
        public double Price(double S,double r, double vol, double t, double ep)
        {
            double price;
            double deltaT = t / Convert.ToDouble(Input.step);
            price = S * Math.Exp((r - Math.Pow(vol,2) / 2) * deltaT + vol * Math.Sqrt(deltaT) * ep);
            return price;
        }
        
    }

    public class Simulator
    {
        SimulationPrice simulationprice = new SimulationPrice();
        public static UInt32 trials { get; set; }
        public List<double> PriceList;
        public static double[,] RandomList;

        public void Initial()
        {
            simulationprice.optionprice = new double[Input.step+1];
            PriceList = new List<double>();
        }
        public void RandomGenerated()
        {
            RandomList = new double[trials, Input.step];
            for (UInt32 i = 0; i < trials; i++)
            {
                for (int k = 0; k < Input.step; k++)
                {
                    RandomList[i,k] = GaussianBoxMuller.NextDouble();
                }
            }
        }
        public double CallPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < trials; i++)
            {
                simulationprice.optionprice[0] = S;
                for (int j = 1; j < Input.step+1; j++)
                {
                    simulationprice.optionprice[j] = simulationprice.Price(simulationprice.optionprice[j - 1], r, vol, t, RandomList[i,j-1]);
                }
                PriceList.Add(simulationprice.optionprice.Last());
            }
            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < PriceList.Count; i++)
            {
                Option.Add(Math.Max(PriceList[(int)i] - Input.K, 0));
            }
            double price=0;
            price = Math.Exp(-r * t)*Option.Average();
            return price;
        }
        public double PutPrice(double S, double r, double vol, double t)
        {
            Initial();
            for (UInt32 i = 0; i < trials; i++)
            {
                simulationprice.optionprice[0] = S;
                for (uint j = 1; j < Input.step+1; j++)
                {
                    simulationprice.optionprice[j] = simulationprice.Price(simulationprice.optionprice[j - 1], r, vol, t,RandomList[i,j-1]);
                }
                PriceList.Add(simulationprice.optionprice.Last());
            }

            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < PriceList.Count; i++)
            {
                Option.Add(Math.Max(Input.K - PriceList[(int)i] , 0));
            }
            double price = 0;
            price = Math.Exp(-r * t) * Option.Average();
            return price;
        }
        public double CallStandardError()
        {
            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < PriceList.Count; i++)
            {
                Option.Add(Math.Max(PriceList[(int)i] - Input.K, 0));
            }
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
        public double PutStandardError()
        {
            List<double> Option = new List<double>();
            for (UInt32 i = 0; i < PriceList.Count; i++)
            {
                Option.Add(Math.Max(Input.K - PriceList[(int)i], 0));
            }
            double avg = Option.Average();
            double sum = Option.Sum(v => Math.Pow(v - avg, 2));
            return (Math.Sqrt(sum / (Option.Count - 1))) / (Math.Sqrt(Option.Count));
        }
    }

    public class Greek
    {
       public double calldelta(double S,double r,double vol,double t)
        {
            Simulator sim = new Simulator();
            double cp1 = sim.CallPrice(S + 0.0001, r, vol, t);
            double cp2 = sim.CallPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double callgamma(double S,double r,double vol,double t)
        {
            return (calldelta(S + 0.01, r, vol, t) - calldelta(S, r, vol, t)) / 0.01 ;
        }
        public double callvega(double S, double r, double vol, double t)
        {
            Simulator sim = new Simulator();
            double cp1 = sim.CallPrice(S , r, vol+0.0001, t);
            double cp2 = sim.CallPrice(S , r, vol-0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double calltheta(double S,double r,double vol,double t)
        {
            Simulator sim = new Simulator();
            double cp1 = sim.CallPrice(S , r, vol, t+0.0001);
            double cp2 = sim.CallPrice(S , r, vol, t-0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double callrho(double S, double r, double vol, double t)
        {
            Simulator sim = new Simulator();
            double cp1 = sim.CallPrice(S, r+0.0001, vol, t );
            double cp2 = sim.CallPrice(S, r-0.0001, vol, t );
            return (cp1 - cp2) / 0.0002;
        }
        public double putdelta(double S, double r, double vol, double t)
        {
            Simulator sim = new Simulator();
            double cp1 = sim.PutPrice(S + 0.0001, r, vol, t);
            double cp2 = sim.PutPrice(S - 0.0001, r, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double putgamma(double S, double r, double vol, double t)
        {
            return (putdelta(S + 0.01, r, vol, t) - putdelta(S, r, vol, t)) / 0.01;
        }
        public double putvega(double S, double r, double vol, double t)
        {
            Simulator sim = new Simulator();
            double cp1 = sim.PutPrice(S, r, vol + 0.0001, t);
            double cp2 = sim.PutPrice(S, r, vol - 0.0001, t);
            return (cp1 - cp2) / 0.0002;
        }
        public double puttheta(double S, double r, double vol, double t)
        {
            Simulator sim = new Simulator();
            double cp1 = sim.PutPrice(S, r, vol, t + 0.0001);
            double cp2 = sim.PutPrice(S, r, vol, t - 0.0001);
            return (cp1 - cp2) / 0.0002;
        }
        public double putrho(double S, double r, double vol, double t)
        {
            Simulator sim = new Simulator();
            double cp1 = sim.PutPrice(S, r + 0.0001, vol, t);
            double cp2 = sim.PutPrice(S, r - 0.0001, vol, t);
            return (cp1 - cp2) / 0.0002;
        }
    }
    internal static class GaussianBoxMuller
    {
        public static double NextDouble()
        {
            double x, y, square;

            do
            {
                x = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                y = 2 * RandomProvider.GetThreadRandom().NextDouble() - 1;
                square = (x * x) + (y * y);
            } while (square >= 1);

            return x * Math.Sqrt(-2 * Math.Log(square) / square);
        }
    }
    internal static class RandomProvider
    {
        private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(() =>
            new Random(Interlocked.Increment(ref seed))
        );

        private static int seed = Environment.TickCount*10;

        public static Random GetThreadRandom()
        {
            return randomWrapper.Value;
        }
    }
}
